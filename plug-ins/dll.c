/* dll.c
// Makes plug-in into dll
// Copyright Dec 26, 2002, Robin.Rowe@MovieEditor.com
// License MIT (http://opensource.org/licenses/mit-license.php)
*/

#ifdef WIN32

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include "../lib/plugin_main.h"
#include "../lib/wire/wire.h"
#include "../app/plug_in.h"

extern GPlugInInfo PLUG_IN_INFO;

static WireBuffer* wire_buffer;

BOOL APIENTRY 
DllMain(HANDLE hModule,DWORD ul_reason_for_call,LPVOID lpReserved)
{    return TRUE;
}

__declspec( dllexport )
int plugin_main(LPVOID data)
{/* FYI:
	progname = argv[0];
	_readfd = atoi (argv[2]);
	_writefd = atoi (argv[3]);
*/
	int argc;
	char** argv;
	PlugIn* plug_in;
	plug_in=(PlugIn*)data;
	plug_in->PLUG_IN_INFO=&PLUG_IN_INFO;
	argc=PLUG_IN_ARGS;
	argv=plug_in->args;
	wire_buffer=plug_in->wire_buffer;
	return gimp_main(argc,argv);
}

#endif
